import React, { Component } from 'react'
import { Layout } from 'antd'
import s from './LoginPage.module.scss'
import { Form, Input, Button, Checkbox } from 'antd';
import FirebaseContext from '../../context/firebaseContext';

const {Content} = Layout

class LoginPage extends Component {

    onFinish = ({email, password, register}) => {
        const {history} = this.props
        if (!register) {
            const {signWithEmail} = this.context
            signWithEmail(email, password)
                .then((res) => {
                    localStorage.setItem('user', res.user.uid)
                    history.push('/')
                }
            )
        } else {
            const {createNewUser} = this.context;
            createNewUser(email, password)
                .then(res => {
                    localStorage.setItem('user', res.user.uid)
                    history.push('/')
                })
        }
    }

    onFinishFailed = (error) => {
        console.log("###OnFinishFailed")
    }

    renderForm = () => {

        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          };
          const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
          };

        return (
            <div>
                <Layout>
                    <Content>
                        <div className={s.root}>
                            <div className={s.form_wrap}>
                                <Form
                                    {...layout}
                                    name="basic"
                                    initialValues={{ register: false }}
                                    onFinish={this.onFinish}
                                    onFinishFailed={this.onFinishFailed}
                                    >
                                    <Form.Item
                                        label="Username"
                                        name="email"
                                        rules={[{ required: true, message: 'Please input your email!' }]}
                                    >
                                        <Input />
                                    </Form.Item>

                                    <Form.Item
                                        label="Password"
                                        name="password"
                                        rules={[{ required: true, message: 'Please input your password!' }]}
                                    >
                                        <Input.Password />
                                    </Form.Item>

                                    <Form.Item {...tailLayout} name="register" valuePropName="checked">
                                        <Checkbox>Register New User</Checkbox>
                                    </Form.Item>

                                    <Form.Item {...tailLayout}>
                                        <Button type="primary" htmlType="submit">
                                        Submit
                                        </Button>
                                    </Form.Item>
                                </Form>
                            </div>
                        </div>
                    </Content>
                </Layout>
            </div>
        )
    }

    render () {
        return this.renderForm()
    }
}

LoginPage.contextType = FirebaseContext

export default LoginPage