import React, { Component } from 'react';

import { ClockCircleOutlined, HomeOutlined, SmileOutlined } from '@ant-design/icons';
import BackgroundBlock from '../../components/BackgroundBlock';
import CardList from '../../components/CardList';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import Paragraph from '../../components/Paragraph';
import Section from '../../components/Section';

// import { wordsList } from '../../wordsList';

import firstBackground from '../../assets/background.jpg';
import secondBackground from '../../assets/back2.jpg';

import s from './HomePage.module.scss';
import getTranslateWord from '../../services/dictionary';
import { Input, Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import FirebaseContext from '../../context/firebaseContext';


class HomePage extends Component {
    
    state = {
        words: [],
        english: '',
        russian: ''
    }

    componentDidMount() {

        this.context.getUserCardsRef().on('value', res => {
            this.setState({
                words: res.val() || []
            })
        })
    }

    submitHandler = (e) => {
        e.preventDefault();
        const obj = {
            id: (this.state.words.length + 1) + "",
            eng: this.state.english,
            rus: this.state.russian
        }
        const array = [...this.state.words, obj]
        
        this.context.getUserCardsRef().set(array)
        .then(() => {
            this.setState({
                words: array,
                english: '',
                russian: ''
            })
        }).catch((err) => {
            console.error('Firebase synchronisation failed');
        });
    }

    onTranslateHandler = async () => {
        const word = await getTranslateWord(this.state.english)
        this.setState({
            russian: word
        })
    }

    onEnglishChangeHandler = (e) => {
        this.setState({
            english: e.target.value
        })
    }

    onRussianChangeHandler = (e) => {
        this.setState({
            russian: e.target.value
        })
    }

    onRemoveClickHandler = (id) => {
        const {words} = this.state
        const newArr = words.filter(word => word.id !== id)
        this.context.getUserCardsRef().set(newArr)
    }

    onLogoutHandler = () => {
        this.props.logout();
    }


    render() {

        const {words} = this.state

        return (
            <>
                <BackgroundBlock
                    backgroundImg={firstBackground}
                    fullHeight
                >
                    <Header white>
                        Время учить слова онлайн
                    </Header>
                    <Paragraph white>
                        Используйте карточки для запоминания и пополняйте активный словарный запас.
                    </Paragraph>
                </BackgroundBlock>
                <Section className={s.textCenter}>
                    <Header size="l">
                        Мы создали уроки, чтобы помочь вам увереннее разговаривать на английском языке
                    </Header>
                    <div className={s.motivation}>
                        <div className={s.motivationBlock}>
                            <div className={s.icons}>
                                <ClockCircleOutlined /> 
                            </div>
                            <Paragraph small>
                                Учитесь, когда есть свободная минутка
                            </Paragraph>
                        </div>

                        <div className={s.motivationBlock}>
                            <div className={s.icons}>
                                <HomeOutlined />
                            </div>
                            <Paragraph small>
                                Откуда угодно — дома, в&nbsp;офисе, в&nbsp;кафе
                            </Paragraph>
                        </div>

                        <div className={s.motivationBlock}>
                            <div className={s.icons}>
                                <SmileOutlined />
                            </div>
                            <Paragraph small>
                                Разговоры по-английски без&nbsp;неловких пауз и&nbsp;«mmm,&nbsp;how&nbsp;to&nbsp;say…»
                            </Paragraph>
                        </div>
                    </div>
                </Section>
                <Section bgColor="#f0f0f0" className={s.textCenter}>
                    <Button onClick={this.onLogoutHandler}>
                        Logout
                    </Button>
                    <Header size='l'>
                        Начать учить английский просто
                    </Header>
                    <Paragraph>
                        Клика по карточкам и узнавай новые слова, быстро и легко!
                    </Paragraph>

                    <form className={s.form} onSubmit={this.submitHandler}>
                        <Input placeholder="Write english word" 
                            onChange={this.onEnglishChangeHandler} 
                            value={this.state.english}
                        />
                        <Button type="primary" 
                            icon={<SearchOutlined />} 
                            disabled={!this.state.english.length}
                            onClick={this.onTranslateHandler}
                        >
                            Translate
                        </Button>
                        <input type="submit" 
                            value="Add the Word" 
                            disabled={!this.state.english.length || !this.state.russian.length}    
                        />
                        <label>
                            Russian: <Input onChange={this.onRussianChangeHandler} value={this.state.russian}/>
                        </label>
                    </form>
                    <CardList 
                        items={words}
                        onDeletedItem={(id) => this.onRemoveClickHandler(id)}
                    />
                </Section>
                <BackgroundBlock
                    backgroundImg={secondBackground}
                >
                    <Header size="l" white>
                        Изучайте английский с персональным сайтом помощником
                    </Header>
                    <Paragraph white>
                        Начните прямо сейчас
                    </Paragraph>
                    <Button>
                        Начать бесплатный урок
                    </Button>
                </BackgroundBlock>
                <Footer/>
            </>
        );
    }
}

HomePage.contextType = FirebaseContext

export default HomePage
