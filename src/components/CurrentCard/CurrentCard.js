import React, { PureComponent } from 'react'
import FirebaseContext from '../../context/firebaseContext';

import s from './CurrentCard.module.scss'
import { Typography, Spin } from 'antd';
import Card from '../Card';
const {Title} = Typography;

class CurrentCard extends PureComponent {

    state = {
        word: {
            _id: 0,
            eng: '',
            rus: ''
        }
    }

    componentDidMount() {
        const {match: {params}} = this.props
        console.log('#### curPage', this.props)
        if(params.id) {
            this.context.getUserCurrentCardRef(params.id).once('value').then(res => {
                this.setState({
                    word: res.val()
                })
            })
        }
    }

    render() {
        const {word: {eng, rus}} = this.state

        if(eng === '' && rus === '') {
            return <div className={s.root}><Spin/></div>
        }

        return(
            <div className={s.root}>
                <Title>
                    This is our Current Card - {eng}
                </Title>    
                <Card eng={eng} rus={rus} />
            </div>
        )
    }
}

CurrentCard.contextType = FirebaseContext

export default CurrentCard