import React, { Component } from 'react';
import HomePage from './components/HomePage/HomePage';
import LoginPage from './components/LoginPage/LoginPage';
import { Spin, Layout } from 'antd';
import s from './App.module.scss'
import FirebaseContext from './context/firebaseContext';
import {BrowserRouter, Route, Link, Switch} from 'react-router-dom'
import { PrivateRoute } from './utils/privateRoute';
import AboutPage from './components/AboutPage/AboutPage';
import CurrentCard from './components/CurrentCard/CurrentCard';

const { Content } = Layout;

class App extends Component {   
    state = {
        user: null
    }

    componentDidMount() {

        if (!this.state.user) {
            const {auth, setUserUid} = this.context
            auth.onAuthStateChanged((user) => {
                if (user) {
                    setUserUid(user.uid)
                    localStorage.setItem('user', JSON.stringify(user.uid))
                    this.setState({
                        user
                    })
                } else {
                    setUserUid(null)
                    localStorage.removeItem('user')
                    this.setState ({
                        user: false
                    })
                }
            })
        }
    }

    logout = () => {
        this.setState({
            user: false
        })
    }

    login = (user) => {
        this.setState({
            user
        })
    }

    render() {

        const {user} = this.state

        if (user === null) {
            return (
                <div className={s.loader_wrap}>
                    <Spin size="large" />
                </div>
            )
        }

        return (
            <>
               <BrowserRouter>
                    <Route path="/login" component={LoginPage} />
                    <Route render={(props) => {
                        const {history:{push}} = props;
                        return (<Layout>
                            <nav>
                                <ul>
                                    <li className={s.crumb}><Link to="/">Home</Link></li>
                                    <li className={s.crumb}><Link to="/about">About</Link></li>
                                </ul>
                            </nav>
                            <Content>
                                <Switch>
                                    <PrivateRoute path="/" exact component={HomePage} />
                                    <PrivateRoute path="/home" component={HomePage} />
                                    <PrivateRoute path="/word/:id?" component={CurrentCard} />
                                    <PrivateRoute path="/about" component={AboutPage} />
                                </Switch>
                            </Content>
                        </Layout>)
                    }} />
                    
               </BrowserRouter>
            </>
        )
    }
}

App.contextType = FirebaseContext

export default App;