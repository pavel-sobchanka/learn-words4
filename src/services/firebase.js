import * as firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

export const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIRE_BASE_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_ID
  };


class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.auth = firebase.auth();
    this.database = firebase.database();

    this.userUid = null
  }

  setUserUid = (uid) => this.userUid = uid;

  signWithEmail = (email, password) => this.auth.signInWithEmailAndPassword(email, password);

  createNewUser = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);

  getUserCardsRef = () => this.database.ref(`/Cards/${this.userUid}`);

  getUserCurrentCardRef = (id) => this.database.ref(`/Cards/${this.userUid}/${id}`)
}  

export default Firebase